#!/bin/sh

# Script to set up the laptops and installs some programs.
# Copyright (C) 2022  Felix Stärk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

# Add admin user for Marc
sudo adduser mnieper
sudo usermod -aG sudo mnieper

# Add guest user
sudo adduser guest

# Enable firewall
sudo systemctl enable ufw
sudo ufw default deny
sudo ufw enable

# Install programs

sudo add-apt-repository ppa:deadsnakes/ppa
sudo add-apt-repository ppa:plt/racket
sudo apt update && sudo apt upgrade

sudo apt install \
    build-essential \
    nano \
    emacs \
    vim \
    git \
    tree \
    texlive-full \
    biber \
    texstudio \
    gimp \
    inkscape \
    vlc \
    sagemath \
    chezscheme \
    racket \
    python3.11

sudo pip install \
    numpy \
    scipy \
    matplotlib \
    qiskit \
    pygame \
    beautifulsoup4 \
    requests \
    cryptography \
    networkx \
    pandas \
    jupyter

sudo snap install code --classic
sudo snap install zoom-client
sudo snap install chromium

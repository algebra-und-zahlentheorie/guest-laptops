# Setup guest laptops

**Deprecated**: The config has replaced by [https://gitlab.com/mathezirkel/management/laptops](https://gitlab.com/mathezirkel/management/laptops).

## Installation manual

1. Install Ubuntu on the laptop.
    * Create an admin account. Note that mnieper will be added as an admin later.
    * Currently, the guest laptops are named by the planets of our solar system.
2. Connect the laptop to internet, e.g. Bayern WLAN.
3. Execute `./configure-eduroam.sh` to be able to connect to eduroam.
4. Execute `./install-guest-laptops.sh` to configure the laptop and install some software.
5. Label the laptop with the corresponding label in [labels.odt](.\labels.odt).

## Using eduroam

To connect to eduroam, execute the script `connect-eduroam.py` which is linked to the desktop and follow the instructions.

Before you the shutdown the PC, do not forget to execute the script `disconnect-eduroam.py` which is also linked to the desktop. Otherwise the eduroam credentials can be used by other users.

## License

This project is licenced under GPL-3.0, see [LICENSE](./LICENSE).

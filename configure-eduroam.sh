#!/bin/sh

# Script to set up eduroam connection.
# Copyright (C) 2022  Felix Stärk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

# Symlink python to python3
sudo ln -sv /usr/bin/python3 /usr/bin/python

# Download eduroam configuration script
mkdir -p $HOME/.local/share/eduroam/
wget www.uni-augsburg.de/linux -O $HOME/.local/share/eduroam/connect-eduroam.py
chmod u+x $HOME/.local/share/eduroam/connect-eduroam.py

# Link configuration script to Desktop
ln -sv $HOME/.local/share/eduroam/connect-eduroam.py $XDG_DESKTOP_DIR/connect-eduroam.py


# Create script to delete eduroam connection
cat > $HOME/.local/share/eduroam/disconnect-eduroam.sh <<EOF
#!/bin/sh
nmcli connection delete eduroam

EOF

chmod u+x $HOME/.local/share/eduroam/disconnect-eduroam.sh

# Link script which deletes eduroam connection to Desktop
ln -sv $HOME/.local/share/eduroam/disconnect-eduroam.sh $XDG_DESKTOP_DIR/disconnect-eduroam.sh
